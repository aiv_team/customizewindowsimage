﻿###############################################################################
### Проверка прав
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "Недостаточно прав для выполнения этого скрипта. Откройте консоль PowerShell с правами администратора и запустите скрипт еще раз"
    Break
}

# Имя файла образа системы
$imageFileNames = @('install.wim', 'install.esd')
# Папка для копирования файлов образа
$isoCopyFolder = Join-Path $PSScriptRoot 'isoCopy\'
# Папка для подключения образа
$offlineFolder = Join-Path $PSScriptRoot 'offline\'
# Папка со скриптами
$scriptsFolder = Join-Path $PSScriptRoot 'scripts'


################################################################################
# Размонтирование образа ОС
function dismountImage {
    param (
        $path = $null
    )


    if ([string]::IsNullOrWhiteSpace($path)) {
        throw [System.IO.FileNotFoundException] 'Не указан путь к смонтированному образа ОС'
    }

    Write-Host 'Размонтируем образ ОС ' -NoNewline
    Write-Host $offlineFolder -ForegroundColor Cyan -NoNewline
    Write-Host '...'
    Dismount-WindowsImage -Path $offlineFolder -Discard
    Remove-Item -Path $offlineFolder -Recurse -Force
}


################################################################################
# Обработка образа ОС
function processImage {
    param (
        $path = $null
    )

    if ([string]::IsNullOrWhiteSpace($path)) {
        throw [System.IO.FileNotFoundException] 'Не указан путь к смонтированному образа ОС'
    }

    ############################################################################
    # Обработка возможностей Windows
    $windowsCapabilities = & "$scriptsFolder\Select-WindowsCapability.ps1" -path $offlineFolder
}


# Load UX/UI resources
Add-Type -AssemblyName System.Windows.Forms

<#
try {
    $imageFileFullName = & "$scriptsFolder\Select-WimImageFile.ps1" -workFolder $PSScriptRoot -ErrorAction Stop
} catch {
    Write-Error $_.Exception
    Break
}


try {
    $selectedEdition = & "$scriptsFolder\Select-WindowsEdition.ps1" -imageFileFullName $imageFileFullName
    #$selectedEdition = selectEdition $imagesInfo
} catch {
    Write-Error $_.Exception
    Break
}

################################################################################
# Монтирование образа ОС
Write-Host 'Монтируем образ ОС ' -NoNewline
Write-Host $selectedEdition.ImageName -ForegroundColor Cyan -NoNewline
Write-Host '...'
if(Test-Path -Path $offlineFolder) {
    Remove-Item -Path $offlineFolder -Recurse -Force
}
$preventOutput = New-Item -Path $offlineFolder -ItemType Directory

if (Get-ItemPropertyValue -Path $imageFileFullName -Name IsReadOnly) {
    Set-ItemProperty -Path $imageFileFullName -Name IsReadOnly -Value $false
}

try {
    $offlineImage = Mount-WindowsImage -ImagePath $imageFileFullName -Index ($selectedEdition.ImageIndex) -Path $offlineFolder -ErrorAction Stop
} catch {
    Write-Error $_.Exception 
    Write-Host ('Попробуйте очистить информацию о повреждённых образах "Dism /Cleanup-Wim"', [System.Environment]::NewLine)
    Break
}
#>

################################################################################
# Обработка образа ОС
try {
    processImage($offlineFolder)
} catch {
    Write-Error $_.Exception
    Break
} finally {
    #dismountImage($offlineFolder)
}

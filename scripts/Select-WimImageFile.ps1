<#
 # ����� wim/esd ����� ������ ��
 #>

param (
    $workFolder = $null
)


if ([string]::IsNullOrWhiteSpace($workFolder)) {
    throw [System.IO.FileNotFoundException] '�� ������ ������� �����'
}


###############################################################################
### ����� ����� ������ wim/esd
$private:openWimFileDialog = New-Object System.Windows.Forms.OpenFileDialog
$openWimFileDialog.InitialDirectory = $workFolder
$openWimFileDialog.Title = '�������� ���� ������ �� (wim/esd)'
$openWimFileDialog.Filter = 'OS image files (*.wim;*.esd)| *.wim;*.esd'
$private:openWimFileResult = $openWimFileDialog.ShowDialog()

if ($openWimFileResult -eq 'Cancel') {
    throw [System.IO.FileNotFoundException] '���� ������ �� �� ������'
}

return $openWimFileDialog.filename

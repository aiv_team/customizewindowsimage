<#
 # ����� ������������ Windows
 #
 # params:
 # - path ����� ��������������� ������ ��
 #>
param (
    $path = $null
)


if ([string]::IsNullOrWhiteSpace($path)) {
    throw [System.IO.FileNotFoundException] '�� ������ ���� � ��������������� ������ ��'
}


################################################################################
# ������� ����������� �������� ��� ����� �� ����������� Windows � ������ ������
function onListClick ($private:listSource) {
    $local:selectedCapability = $listSource[$_.Index]
    $capabilityDescriptionLabel.Text = $(
        $selectedCapability.DisplayName,
        "�������� �����������: $($selectedCapability.Name)",
        "��������: $($selectedCapability.Description)",
        "DownloadSize = $($selectedCapability.DownloadSize), InstallSize = $($selectedCapability.InstallSize)"
        ) -join [System.Environment]::NewLine
}

################################################################################
# ������� ��������� ������
function processSelected {
    Param (
        [Parameter(Mandatory=$true, Position=0)]
        [array] $allCapabilities,
        [Parameter(Position=1)]
        [array] $selectedCapabilities
    )
    
    forEach ($private:processedCapability in $allCapabilities) {
        $private:needInstall = $false
        forEach ($private:selectedCapability in $selectedCapabilities) {
            if ($selectedCapability -eq $processedCapability.DisplayName) {
                $needInstall = $true
            }
        }

        if ($needInstall -and $processedCapability.State -eq 'NotPresent') {
            $selectionResult.CapabilitiesForInstall += $processedCapability
        }
        if ((-not $needInstall) -and $processedCapability.State -eq 'Installed') {
            $selectionResult.CapabilitiesForRemove += $processedCapability
        }
    }
}


################################################################################
# ����������� Windows, ������� ����� ��������
[string[]] $local:installedCapabilities = Get-Content -Path (Join-Path $PSScriptRoot 'capabilitiesForInstall.txt')

################################################################################
# ����������� Windows, ������� ����� �������
[string[]] $local:uninstalledCapabilities = Get-Content -Path (Join-Path $PSScriptRoot 'capabilitiesForRemove.txt')

################################################################################
# ��������� ������������ Windows
Write-Host '�������� ����������� Windows...'
$local:windowsCapabilities = @()
Get-WindowsCapability -Name * -Path $path | ForEach-Object -Process {
    $windowsCapabilities += [pscustomobject]@{
        Name = $_.Name
        DisplayName = $_.DisplayName
        Description = $_.Description
        State = $_.State
        DownloadSize = $_.DownloadSize
        InstallSize = $_.InstallSize
    }
}


$local:basicLanguages = @()
$local:fontsLanguages = @()
$local:handwritingLanguages = @()
$local:ocrLanguages = @()
$local:speechLanguages = @()
$local:textToSpeechLanguages = @()
$local:rsats = @()
$local:commonCapabilities = @()
forEach ($windowsCapability in $windowsCapabilities) {
    $private:capabilityName = $windowsCapability.Name

    $private:needUninstall = $false
    forEach ($uninstalledCapability in $uninstalledCapabilities) {
        if ($capabilityName -like [string]::Concat($uninstalledCapability, '*')) {
            $needUninstall = $true
            Break
        }
    }
    $private:needInstall = $false
    forEach ($installedCapability in $installedCapabilities) {
        if ($capabilityName -like [string]::Concat($installedCapability, '*')) {
            $needInstall = $true
            Break
        }
    }
    $private:willInstalled = $needInstall -or (-not ($needUninstall -or ($windowsCapability.State -eq 'NotPresent')))
    $windowsCapability | Add-Member -NotePropertyName WillInstall -TypeName Boolean -NotePropertyValue $willInstalled

    if ($capabilityName -like 'Language.Basic*') {
        $basicLanguages += $windowsCapability
    } elseif ($capabilityName -like 'Language.Fonts*') {
        $fontsLanguages += $windowsCapability
    } elseif ($capabilityName -like 'Language.Handwriting*') {
        $handwritingLanguages += $windowsCapability
    } elseif ($capabilityName -like 'Language.OCR*') {
        $ocrLanguages += $windowsCapability
    } elseif ($capabilityName -like 'Language.Speech*') {
        $speechLanguages += $windowsCapability
    } elseif ($capabilityName -like 'Language.TextToSpeech*') {
        $textToSpeechLanguages += $windowsCapability
    } elseif ($capabilityName -like 'Rsat.*') {
        $rsats += $windowsCapability
    } else {
        $commonCapabilities += $windowsCapability
    }
}

$basicLanguages = $basicLanguages | Sort-Object -Property DisplayName
$fontsLanguages = $fontsLanguages | Sort-Object -Property DisplayName
$handwritingLanguages = $handwritingLanguages | Sort-Object -Property DisplayName
$ocrLanguages = $ocrLanguages | Sort-Object -Property DisplayName
$speechLanguages = $speechLanguages | Sort-Object -Property DisplayName
$textToSpeechLanguages = $textToSpeechLanguages | Sort-Object -Property DisplayName
$rsats = $rsats | Sort-Object -Property DisplayName
$commonCapabilities = $commonCapabilities | Sort-Object -Property DisplayName

<#
Write-Host ([System.Environment]::NewLine, 'basicLanguages:')
forEach ($windowsCapability in $basicLanguages) {
    Write-Host $windowsCapability
}

Write-Host ([System.Environment]::NewLine, 'fontsLanguages:')
forEach ($windowsCapability in $fontsLanguages) {
    Write-Host $windowsCapability
}

Write-Host ([System.Environment]::NewLine, 'handwritingLanguages:')
forEach ($windowsCapability in $handwritingLanguages) {
    Write-Host $windowsCapability
}

Write-Host ([System.Environment]::NewLine, 'ocrLanguages:')
forEach ($windowsCapability in $ocrLanguages) {
    Write-Host $windowsCapability
}

Write-Host ([System.Environment]::NewLine, 'speechLanguages:')
forEach ($windowsCapability in $speechLanguages) {
    Write-Host $windowsCapability
}

Write-Host ([System.Environment]::NewLine, 'textToSpeechLanguages:')
forEach ($windowsCapability in $textToSpeechLanguages) {
    Write-Host $windowsCapability
}

Write-Host ([System.Environment]::NewLine, 'rsats:')
forEach ($windowsCapability in $rsats) {
    Write-Host $windowsCapability
}
#>

<#
Write-Host ([System.Environment]::NewLine, 'commonCapabilities:')
forEach ($windowsCapability in $commonCapabilities) {
    Write-Host $windowsCapability
}
#>

<#
Write-Host ([System.Environment]::NewLine, '������ ������ ������������:')
forEach ($windowsCapability in $windowsCapabilities) {
    Write-Host $windowsCapability
}
#>


################################################################################
# ���������� ���� ������ ������������ Windows
$local:formWidth = 1100
$local:boxHeight = 138
$local:labelWidth = $formWidth / 2 - 50
$local:listBoxWidth = $formWidth / 2 - 30

$local:selectCapabilityForm = New-Object System.Windows.Forms.Form
$selectCapabilityForm.Text = '�������� ����������� ��'
$selectCapabilityForm.FormBorderStyle = 'Fixed3D'
$selectCapabilityForm.MaximizeBox = $false
$selectCapabilityForm.Size = New-Object System.Drawing.Size(($formWidth + 10), 800)

##### �������� �����������
$local:commonCapabilitiesLabel = New-Object System.Windows.Forms.Label
$commonCapabilitiesLabel.Location = New-Object System.Drawing.Point(30, 20)
$commonCapabilitiesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$commonCapabilitiesLabel.Text = '�������� �����������:'
$selectCapabilityForm.Controls.Add($commonCapabilitiesLabel)

$local:commonCapabilitiesListBox = New-Object System.Windows.Forms.CheckedListBox
$commonCapabilitiesListBox.Location = New-Object System.Drawing.Point(($commonCapabilitiesLabel.Left - 10), ($commonCapabilitiesLabel.Bottom))
$commonCapabilitiesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $commonCapabilities) {
    $local:newItem = $commonCapabilitiesListBox.Items.Add($capability.DisplayName)
    $commonCapabilitiesListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$commonCapabilitiesListBox.CheckOnClick = $true
$commonCapabilitiesListBox.Add_ItemCheck({onListClick $commonCapabilities})
$selectCapabilityForm.Controls.Add($commonCapabilitiesListBox)

##### �������� ���������� ����������������� ������� (Remote Server Administration Tools)
$local:rsatsLabel = New-Object System.Windows.Forms.Label
$rsatsLabel.Location = New-Object System.Drawing.Point(($formWidth / 2 + 20), 20)
$rsatsLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$rsatsLabel.Text = '�������� ���������� ����������������� �������:'
$selectCapabilityForm.Controls.Add($rsatsLabel)

$local:rsatsListBox = New-Object System.Windows.Forms.CheckedListBox
$rsatsListBox.Location = New-Object System.Drawing.Point(($rsatsLabel.Left - 10), ($rsatsLabel.Bottom))
$rsatsListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $rsats) {
    $local:newItem = $rsatsListBox.Items.Add($capability.DisplayName)
    $rsatsListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$rsatsListBox.CheckOnClick = $true
$rsatsListBox.Add_ItemCheck({onListClick $rsats})
$selectCapabilityForm.Controls.Add($rsatsListBox)

##### �������� �����������
$local:languageGroupBox = New-Object System.Windows.Forms.GroupBox
$languageGroupBox.Location = New-Object System.Drawing.Point(10, ($commonCapabilitiesListBox.Bottom))
$languageGroupBox.Size = New-Object System.Drawing.Size(($formWidth - 20), (($commonCapabilitiesListBox.Bottom - $commonCapabilitiesLabel.Top) * 3 + 20))
$languageGroupBox.Text = '�������� �����������'
$selectCapabilityForm.Controls.Add($languageGroupBox)

##### �����
$local:basicLanguagesLabel = New-Object System.Windows.Forms.Label
$basicLanguagesLabel.Location = New-Object System.Drawing.Point(20, 20)
$basicLanguagesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$basicLanguagesLabel.Text = '�����:'
$languageGroupBox.Controls.Add($basicLanguagesLabel)

$local:basicLanguagesListBox = New-Object System.Windows.Forms.CheckedListBox
$basicLanguagesListBox.Location = New-Object System.Drawing.Point(($basicLanguagesLabel.Left - 10), ($basicLanguagesLabel.Bottom))
$basicLanguagesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $basicLanguages) {
    $local:newItem = $basicLanguagesListBox.Items.Add($capability.DisplayName)
    $basicLanguagesListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$basicLanguagesListBox.CheckOnClick = $true
$basicLanguagesListBox.Add_ItemCheck({onListClick $basicLanguages})
$languageGroupBox.Controls.Add($basicLanguagesListBox)

##### ������
$local:fontsLanguagesLabel = New-Object System.Windows.Forms.Label
$fontsLanguagesLabel.Location = New-Object System.Drawing.Point(($formWidth / 2 + 10), 20)
$fontsLanguagesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$fontsLanguagesLabel.Text = '������:'
$languageGroupBox.Controls.Add($fontsLanguagesLabel)

$local:fontsLanguagesListBox = New-Object System.Windows.Forms.CheckedListBox
$fontsLanguagesListBox.Location = New-Object System.Drawing.Point(($fontsLanguagesLabel.Left - 10), ($fontsLanguagesLabel.Bottom))
$fontsLanguagesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $fontsLanguages) {
    $local:newItem = $fontsLanguagesListBox.Items.Add($capability.DisplayName)
    $fontsLanguagesListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$fontsLanguagesListBox.CheckOnClick = $true
$fontsLanguagesListBox.Add_ItemCheck({onListClick $fontsLanguages})
$languageGroupBox.Controls.Add($fontsLanguagesListBox)

##### ���������� ����
$local:handwritingLabel = New-Object System.Windows.Forms.Label
$handwritingLabel.Location = New-Object System.Drawing.Point(($basicLanguagesLabel.Left), ($basicLanguagesListBox.Bottom))
$handwritingLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$handwritingLabel.Text = '���������� ����:'
$languageGroupBox.Controls.Add($handwritingLabel)

$local:handwritingListBox = New-Object System.Windows.Forms.CheckedListBox
$handwritingListBox.Location = New-Object System.Drawing.Point(($basicLanguagesListBox.Left), ($handwritingLabel.Bottom))
$handwritingListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $handwritingLanguages) {
    $local:newItem = $handwritingListBox.Items.Add($capability.DisplayName)
    $handwritingListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$handwritingListBox.CheckOnClick = $true
$handwritingListBox.Add_ItemCheck({onListClick $handwritingLanguages})
$languageGroupBox.Controls.Add($handwritingListBox)

##### ������������� ������
$local:ocrLanguagesLabel = New-Object System.Windows.Forms.Label
$ocrLanguagesLabel.Location = New-Object System.Drawing.Point(($fontsLanguagesLabel.Left), ($fontsLanguagesListBox.Bottom))
$ocrLanguagesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$ocrLanguagesLabel.Text = '������������� ������:'
$languageGroupBox.Controls.Add($ocrLanguagesLabel)

$local:ocrLanguagesListBox = New-Object System.Windows.Forms.CheckedListBox
$ocrLanguagesListBox.Location = New-Object System.Drawing.Point(($fontsLanguagesListBox.Left), ($ocrLanguagesLabel.Bottom))
$ocrLanguagesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $ocrLanguages) {
    $local:newItem = $ocrLanguagesListBox.Items.Add($capability.DisplayName)
    $ocrLanguagesListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$ocrLanguagesListBox.CheckOnClick = $true
$ocrLanguagesListBox.Add_ItemCheck({onListClick $ocrLanguages})
$languageGroupBox.Controls.Add($ocrLanguagesListBox)

##### ��������� �������
$local:speechLanguagesLabel = New-Object System.Windows.Forms.Label
$speechLanguagesLabel.Location = New-Object System.Drawing.Point(($handwritingLabel.Left), ($handwritingListBox.Bottom))
$speechLanguagesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$speechLanguagesLabel.Text = '��������� �������:'
$languageGroupBox.Controls.Add($speechLanguagesLabel)

$local:speechLanguagesListBox = New-Object System.Windows.Forms.CheckedListBox
$speechLanguagesListBox.Location = New-Object System.Drawing.Point(($handwritingListBox.Left), ($speechLanguagesLabel.Bottom))
$speechLanguagesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $speechLanguages) {
    $local:newItem = $speechLanguagesListBox.Items.Add($capability.DisplayName)
    $speechLanguagesListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$speechLanguagesListBox.CheckOnClick = $true
$speechLanguagesListBox.Add_ItemCheck({onListClick $speechLanguages})
$languageGroupBox.Controls.Add($speechLanguagesListBox)

##### ����������� ������
$local:textToSpeechLanguagesLabel = New-Object System.Windows.Forms.Label
$textToSpeechLanguagesLabel.Location = New-Object System.Drawing.Point(($ocrLanguagesLabel.Left), ($ocrLanguagesListBox.Bottom))
$textToSpeechLanguagesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$textToSpeechLanguagesLabel.Text = '����������� ������:'
$languageGroupBox.Controls.Add($textToSpeechLanguagesLabel)

$local:textToSpeechLanguagesListBox = New-Object System.Windows.Forms.CheckedListBox
$textToSpeechLanguagesListBox.Location = New-Object System.Drawing.Point(($ocrLanguagesListBox.Left), ($textToSpeechLanguagesLabel.Bottom))
$textToSpeechLanguagesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight) 
foreach ($private:capability in $textToSpeechLanguages) {
    $local:newItem = $textToSpeechLanguagesListBox.Items.Add($capability.DisplayName)
    $textToSpeechLanguagesListBox.SetItemchecked($newItem, $capability.WillInstall)
}
$textToSpeechLanguagesListBox.CheckOnClick = $true
$textToSpeechLanguagesListBox.Add_ItemCheck({onListClick $textToSpeechLanguages})
$languageGroupBox.Controls.Add($textToSpeechLanguagesListBox)


$local:capabilityDescriptionLabel = New-Object System.Windows.Forms.Label
$capabilityDescriptionLabel.Location = New-Object System.Drawing.Point($languageGroupBox.Left, ($languageGroupBox.Bottom + 10))
$capabilityDescriptionLabel.Size = New-Object System.Drawing.Size(($selectCapabilityForm.Width - $languageGroupBox.Left - 220), ($selectCapabilityForm.Height - $languageGroupBox.Bottom - 20))
$selectCapabilityForm.Controls.Add($capabilityDescriptionLabel)


$local:buttonOk = New-Object System.Windows.Forms.Button
$buttonOk.Location = New-Object System.Drawing.Point(($selectCapabilityForm.Width - 104), ($selectCapabilityForm.Height - 70))
$buttonOk.Size = New-Object System.Drawing.Size(75, 23)
$buttonOk.Text = '�������'
$selectCapabilityForm.Controls.Add($buttonOk)

$buttonOk.Add_Click({
    processSelected -allCapabilities $commonCapabilities -selectedCapabilities $commonCapabilitiesListBox.CheckedItems
    processSelected -allCapabilities $rsats -selectedCapabilities $rsatsListBox.CheckedItems
    processSelected -allCapabilities $basicLanguages -selectedCapabilities $basicLanguagesListBox.CheckedItems
    processSelected -allCapabilities $fontsLanguages -selectedCapabilities $fontsLanguagesListBox.CheckedItems
    processSelected -allCapabilities $handwritingLanguages -selectedCapabilities $handwritingListBox.CheckedItems
    processSelected -allCapabilities $ocrLanguages -selectedCapabilities $ocrLanguagesListBox.CheckedItems
    processSelected -allCapabilities $speechLanguages -selectedCapabilities $speechLanguagesListBox.CheckedItems
    processSelected -allCapabilities $textToSpeechLanguages -selectedCapabilities $textToSpeechLanguagesListBox.CheckedItems

    $selectCapabilityForm.DialogResult = 'OK'
    $selectCapabilityForm.Close()
})
 
$local:buttonCancel = New-Object System.Windows.Forms.Button
$buttonCancel.Location = New-Object System.Drawing.Point(($selectCapabilityForm.Width - 199), ($selectCapabilityForm.Height - 70))
$buttonCancel.Size = New-Object System.Drawing.Size(75, 23)
$buttonCancel.Text = "������"
$selectCapabilityForm.Controls.Add($buttonCancel)

$buttonCancel.Add_Click({
    ([ref]$selectionResult).Value = $null
    $selectCapabilityForm.Close()
})


<#
# ����� ��� ������������ ��� �������� �����
$local:qqqLabel = New-Object System.Windows.Forms.Label
$qqqLabel.Location = New-Object System.Drawing.Point(($formWidth / 2), 5)
$qqqLabel.Size = New-Object System.Drawing.Size(1, 700) 
$qqqLabel.BorderStyle = 'FixedSingle'
$qqqLabel.Text = ''
$selectCapabilityForm.Controls.Add($qqqLabel)

$local:leftSide = 0
do {
    $local:qqqLabel = New-Object System.Windows.Forms.Label
    $qqqLabel.Location = New-Object System.Drawing.Point($local:leftSide, 5)
    $qqqLabel.Size = New-Object System.Drawing.Size(1, (200 + $local:leftSide % 100)) 
    $qqqLabel.BorderStyle = 'FixedSingle'
    $qqqLabel.Text = ''
    $selectCapabilityForm.Controls.Add($qqqLabel)

    $local:qqqLabel = New-Object System.Windows.Forms.Label
    $qqqLabel.Location = New-Object System.Drawing.Point(($formWidth - $local:leftSide), 5)
    $qqqLabel.Size = New-Object System.Drawing.Size(1, (200 + $local:leftSide % 100)) 
    $qqqLabel.BorderStyle = 'FixedSingle'
    $qqqLabel.Text = ''
    $selectCapabilityForm.Controls.Add($qqqLabel)

    $local:leftSide += 10
} while ($local:leftSide -lt ($formWidth / 2))
#>

$local:selectionResult = [pscustomobject]@{
    CapabilitiesForInstall = @()
    CapabilitiesForRemove = @()
}

$selectCapabilityForm.TopMost = $true
$local:formResult = $selectCapabilityForm.ShowDialog((New-Object System.Windows.Forms.Form -Property @{TopMost = $true}))

if (-not $formResult -eq 'OK') {
    Write-Warning '�� ������� ����������� ��'
    throw [System.IO.FileNotFoundException] '�� ������� ����������� ��'
}

return $selectionResult

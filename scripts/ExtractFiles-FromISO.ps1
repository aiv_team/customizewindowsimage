<#
 # ���������� ������ ������ �� �� iso.
 #
 # params:
 # - workFolder ������� �����
 #>

param (
    $workFolder = $null
)


if ([string]::IsNullOrWhiteSpace($workFolder)) {
    throw [System.IO.FileNotFoundException] '�� ������ ������� �����'
}


# ����� ������ ������ �������
$imageFileNames = @('install.wim', 'install.esd')

    
###############################################################################
# ����� ����� iso ������
$local:openIsoFileDialog = New-Object System.Windows.Forms.OpenFileDialog
$openIsoFileDialog.InitialDirectory = $workFolder
$openIsoFileDialog.Title = '�������� iso ���� ������ ��'
$openIsoFileDialog.Filter = 'OS image files (*.iso)| *.iso'
$local:openFileResult = $openIsoFileDialog.ShowDialog()

if ($openFileResult -eq 'Cancel') {
    throw [System.IO.FileNotFoundException] '���� ������ �� �� ������'
}


################################################################################
# ����������� iso ������
Write-Host '���������� �������� iso ����� ' -NoNewline
Write-Host $openIsoFileDialog.filename -ForegroundColor Cyan -NoNewline
Write-Host '...'
$local:moutedIso = Mount-DiskImage -ImagePath ($openIsoFileDialog.filename) -PassThru -ErrorAction Stop | Get-Volume

if ([string]::IsNullOrWhiteSpace($moutedIso.DriveLetter)) {
    throw [System.IO.DriveNotFoundException] '�� ������� ���������� ISO �����'
}
$local:isoVolume = [string]::Concat($moutedIso.DriveLetter, ':\')
Write-Host '���������: ��� ' -NoNewline
Write-Host $isoVolume -ForegroundColor Cyan -NoNewline
Write-Host (", ������", $moutedIso.Size)
$local:sourceIsoFiles = Join-Path $isoVolume "*"
if ([string]::IsNullOrEmpty($sourceIsoFiles)) {
    throw [System.IO.DriveNotFoundException] '�� ������� ���������� ������������ ����� wim ������'
}


################################################################################
# ����������� ������ iso ������
if(Test-Path -Path $isoCopyFolder) {
    Remove-Item -Path $isoCopyFolder -Recurse -Force
}
foreach ($imageFileName in $imageFileNames) {
    $local:targetImageFileName = Join-Path $PSScriptRoot $imageFileName
    if(Test-Path -Path $targetImageFileName) {
        Remove-Item -Path $targetImageFileName -Recurse -Force
    }
}
$preventOutput = New-Item -Path $isoCopyFolder -ItemType Directory
Write-Host '�������� ����� ��������� iso ������ � ' -NoNewline
Write-Host $isoCopyFolder -ForegroundColor Cyan -NoNewline
Write-Host '...'
$local:isoCopied = Copy-Item $sourceIsoFiles $isoCopyFolder -Force -Recurse -PassThru -ErrorAction Stop
Write-Host ('����������� ', $isoCopied.Count, ' ������')


################################################################################
# ���������� iso ������
Write-Host '��������� �������� iso ����� ' -NoNewline
Write-Host $openIsoFileDialog.filename -ForegroundColor Cyan -NoNewline
Write-Host '...'
$local:dismountResult = Dismount-DiskImage -ImagePath ($openIsoFileDialog.filename)
Write-Host '�������� iso ����� ' -NoNewline
Write-Host ($dismountResult.ImagePath) -ForegroundColor Cyan


################################################################################
# ���������� ����� ������ wim ��� esd
$local:sourceImageFileName = $null
foreach ($imageFileName in $imageFileNames) {
    $sourceImageFileName = [string]::Concat($isoCopyFolder, "sources\", $imageFileName)
    if(Test-Path -Path $sourceImageFileName) {
        $targetImageFileName = Join-Path $workFolder $imageFileName
        Write-Host '��������� ���� ������ �� ' -NoNewline
        Write-Host $imageFileName -ForegroundColor Cyan -NoNewline
        Write-Host '...'
        Break
    }
}
if ([string]::IsNullOrWhiteSpace($sourceImageFileName)) {
    throw [System.IO.FileNotFoundException] '�� ������ ���� ������ install.wim(esd)'
}
$local:imageFile = Copy-Item $sourceImageFileName $targetImageFileName -Force -Recurse -PassThru

return $imageFile.FullName
<#
 # ����� ������� ����������
 #
 # params:
 # - path ����� ��������������� ������ ��
 #>
param (
    $path = $null
)


if ([string]::IsNullOrWhiteSpace($path)) {
    throw [System.IO.FileNotFoundException] '�� ������ ���� � ��������������� ������ ��'
}


################################################################################
# ������� ����������� �������� ��� ����� �� ����� ���������� � ������ ������
function onListClick ($private:listSource) {
    $local:selectedPackage = $listSource[$_.Index]
    $packageDescriptionLabel.Text = $(
        $selectedPackage.DisplayName,
        "�������� ������: $($selectedPackage.PackageName)",
        "������: $($selectedPackage.Version)"
    ) -join ([string]::Concat([System.Environment]::NewLine, [System.Environment]::NewLine))
}

################################################################################
# ������� ��������� ������
function processSelected {
    Param (
        [Parameter(Mandatory=$true, Position=0)]
        [array] $allPackages,
        [Parameter(Position=1)]
        [array] $selectedPackages
    )
    
    forEach ($private:processedPackage in $allPackages) {
        $private:needInstall = $false
        forEach ($private:selectedPackage in $selectedPackages) {
            if ($selectedPackage -eq $processedPackage.PackageName) {
                $needInstall = $true
            }
        }

        if ($needInstall) {
            $selectionResult.PackagesForInstall += $processedPackage
        }
        if (-not $needInstall) {
            $selectionResult.PackagesForRemove += $processedPackage
        }
    }
}


################################################################################
# ������ ����������, ������� ����� ��������
[string[]] $local:installedPackages = Get-Content -Path (Join-Path $PSScriptRoot 'provisionedAppxForInstall.txt')

################################################################################
# ������ ����������, ������� ����� �������
[string[]] $local:uninstalledPackages = Get-Content -Path (Join-Path $PSScriptRoot 'provisionedAppxForRemove.txt')

################################################################################
# ��������� ������� ����������
Write-Host '�������� ������ ����������...'
$local:windowsPackages = @()
Get-AppProvisionedPackage -Path $path | ForEach-Object -Process {
    $windowsPackages += [pscustomobject]@{
        PackageName = $_.PackageName
        DisplayName = $_.DisplayName
        Version = $_.Version
    }
}

forEach ($windowsPackage in $windowsPackages) {
    $private:packageName = $windowsPackage.PackageName

    $private:needUninstall = $false
    forEach ($uninstalledPackage in $uninstalledPackages) {
        if ($packageName -like [string]::Concat($uninstalledPackage, '*')) {
            $needUninstall = $true
            Break
        }
    }

    $private:needInstall = $false
    forEach ($installedPackage in $installedPackages) {
        if ($packageName -like [string]::Concat($installedPackage, '*')) {
            $needInstall = $true
            Break
        }
    }

    $private:willInstalled = $needInstall -or (-not ($needUninstall -or ($windowsPackage.State -eq 'NotPresent')))
    $windowsPackage | Add-Member -NotePropertyName WillInstall -TypeName Boolean -NotePropertyValue $willInstalled

    $private:itemName = $windowsPackage.DisplayName
    $windowsPackage | Add-Member -NotePropertyName ItemName -TypeName String -NotePropertyValue $itemName
}

$windowsPackages = $windowsPackages | Sort-Object -Property DisplayName

<#
Write-Host ([System.Environment]::NewLine, '������ ������ �������:')
forEach ($windowsPackage in $windowsPackages) {
    Write-Host $windowsPackage
}
#>


################################################################################
# ���������� ���� ������ ������� ����������
$local:formWidth = 1100
$local:boxHeight = 600
$local:labelWidth = $formWidth / 2 - 50
$local:listBoxWidth = $formWidth / 2 - 30

$local:selectPackagesForm = New-Object System.Windows.Forms.Form
$selectPackagesForm.Text = '�������� ������ ����������'
$selectPackagesForm.FormBorderStyle = 'Fixed3D'
$selectPackagesForm.MaximizeBox = $false
$selectPackagesForm.Size = New-Object System.Drawing.Size(($formWidth + 10), 800)

##### ������
$local:packagesLabel = New-Object System.Windows.Forms.Label
$packagesLabel.Location = New-Object System.Drawing.Point(30, 20)
$packagesLabel.Size = New-Object System.Drawing.Size($labelWidth, 16) 
$packagesLabel.Text = '������:'
$selectPackagesForm.Controls.Add($packagesLabel)

$local:packagesListBox = New-Object System.Windows.Forms.CheckedListBox
$packagesListBox.Location = New-Object System.Drawing.Point(($packagesLabel.Left - 10), ($packagesLabel.Bottom))
$packagesListBox.Size = New-Object System.Drawing.Size($listBoxWidth, ($selectPackagesForm.Height - 80)) 
foreach ($private:package in $windowsPackages) {
    $local:newItem = $packagesListBox.Items.Add($package.ItemName)
    $packagesListBox.SetItemchecked($newItem, $package.WillInstall)
}
$packagesListBox.CheckOnClick = $true
$packagesListBox.Add_ItemCheck({onListClick $windowsPackages})
$selectPackagesForm.Controls.Add($packagesListBox)


$local:packageDescriptionLabel = New-Object System.Windows.Forms.Label
$packageDescriptionLabel.Location = New-Object System.Drawing.Point(($formWidth / 2 + 20), $packagesListBox.Top)
$packageDescriptionLabel.Size = New-Object System.Drawing.Size($listBoxWidth, $boxHeight)
$selectPackagesForm.Controls.Add($packageDescriptionLabel)


$local:buttonOk = New-Object System.Windows.Forms.Button
$buttonOk.Location = New-Object System.Drawing.Point(($selectPackagesForm.Width - 104), ($selectPackagesForm.Height - 70))
$buttonOk.Size = New-Object System.Drawing.Size(75, 23)
$buttonOk.Text = '�������'
$selectPackagesForm.Controls.Add($buttonOk)

$buttonOk.Add_Click({
    $private:selectedPackages = @()
    forEach ($packageIndex in $packagesListBox.CheckedIndices) {
        $selectedPackages += $windowsPackages[$packageIndex].PackageName
    }

    processSelected -allPackages $windowsPackages -selectedPackages $selectedPackages

    $selectPackagesForm.DialogResult = 'OK'
    $selectPackagesForm.Close()
})
 
$local:buttonCancel = New-Object System.Windows.Forms.Button
$buttonCancel.Location = New-Object System.Drawing.Point(($selectPackagesForm.Width - 199), ($selectPackagesForm.Height - 70))
$buttonCancel.Size = New-Object System.Drawing.Size(75, 23)
$buttonCancel.Text = "������"
$selectPackagesForm.Controls.Add($buttonCancel)

$buttonCancel.Add_Click({
    ([ref]$selectionResult).Value = $null
    $selectPackagesForm.Close()
})


<#
# ����� ��� ������������ ��� �������� �����
$local:qqqLabel = New-Object System.Windows.Forms.Label
$qqqLabel.Location = New-Object System.Drawing.Point(($formWidth / 2), 5)
$qqqLabel.Size = New-Object System.Drawing.Size(1, 700) 
$qqqLabel.BorderStyle = 'FixedSingle'
$qqqLabel.Text = ''
$selectPackagesForm.Controls.Add($qqqLabel)

$local:leftSide = 0
do {
    $local:qqqLabel = New-Object System.Windows.Forms.Label
    $qqqLabel.Location = New-Object System.Drawing.Point($local:leftSide, 5)
    $qqqLabel.Size = New-Object System.Drawing.Size(1, (200 + $local:leftSide % 100)) 
    $qqqLabel.BorderStyle = 'FixedSingle'
    $qqqLabel.Text = ''
    $selectPackagesForm.Controls.Add($qqqLabel)

    $local:qqqLabel = New-Object System.Windows.Forms.Label
    $qqqLabel.Location = New-Object System.Drawing.Point(($formWidth - $local:leftSide), 5)
    $qqqLabel.Size = New-Object System.Drawing.Size(1, (200 + $local:leftSide % 100)) 
    $qqqLabel.BorderStyle = 'FixedSingle'
    $qqqLabel.Text = ''
    $selectPackagesForm.Controls.Add($qqqLabel)

    $local:leftSide += 10
} while ($local:leftSide -lt ($formWidth / 2))
#>

$local:selectionResult = [pscustomobject]@{
    PackagesForInstall = @()
    PackagesForRemove = @()
}

$selectPackagesForm.TopMost = $true
$local:formResult = $selectPackagesForm.ShowDialog((New-Object System.Windows.Forms.Form -Property @{TopMost = $true}))

if (-not $formResult -eq 'OK') {
    Write-Warning '�� ������� ������ ����������'
    throw [System.IO.FileNotFoundException] '�� ������� ������ ����������'
}

return $selectionResult

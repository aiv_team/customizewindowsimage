<#
 # ����� ����������� ���� ������ �������� ��
 #
 # params:
 # - imageFileFullName ������ ���� � wim ����� ������ ��
 #>
param (
    $imageFileFullName = $null
)


if ([string]::IsNullOrWhiteSpace($imageFileFullName)) {
    throw [System.IO.FileNotFoundException] '�� ������ ���� � wim ����� ������ ��'
}


################################################################################
# ��������� ���������� � ��������� � ������ ��
$local:editionList = @()
Get-WindowsImage -ImagePath $imageFileFullName | ForEach-Object -Process {
    $editionList += [pscustomobject]@{
        ImageIndex=$_.ImageIndex
        ImageName=$_.ImageName
        ImageDescription=$_.ImageDescription
        ImagePath=$_.ImagePath
        ImageSize=$_.ImageSize
    }
}


################################################################################
# ���������� ���� ������ �������� ��
$local:selectEditionForm = New-Object System.Windows.Forms.Form
$selectEditionForm.Text = '�������� �������� ��'
$selectEditionForm.FormBorderStyle = 'Fixed3D'
$selectEditionForm.MaximizeBox = $false
$selectEditionForm.Size = New-Object System.Drawing.Size(500, 320)

$local:editionListBox = New-Object System.Windows.Forms.ListBox
$editionListBox.Location = New-Object System.Drawing.Point(20, 20)
$editionListBox.Size = New-Object System.Drawing.Size(($selectEditionForm.Width - 50), 100) 
foreach ($edition in $editionList) {
    $preventOutput = $editionListBox.Items.Add($edition.ImageName)
}
$selectEditionForm.Controls.Add($editionListBox)

$local:descriptionLabel = New-Object System.Windows.Forms.Label
$descriptionLabel.Location = New-Object System.Drawing.Point(($editionListBox.Left), ($editionListBox.Bottom + 3))
$descriptionLabel.Size = New-Object System.Drawing.Size(($selectEditionForm.Width - 50), 17) 
$descriptionLabel.Text = '��������:'
$selectEditionForm.Controls.Add($descriptionLabel)

$local:descriptionField = New-Object System.Windows.Forms.Label
$descriptionField.Location = New-Object System.Drawing.Point(($descriptionLabel.Left), ($descriptionLabel.Bottom))
$descriptionField.Size = New-Object System.Drawing.Size(($selectEditionForm.Width - 50), 110)
$selectEditionForm.Controls.Add($descriptionField)

$editionListBox.add_SelectedIndexChanged({
    $descriptionField.Text = $editionList[$editionListBox.SelectedIndex].ImageDescription
})

$local:buttonOk = New-Object System.Windows.Forms.Button
$buttonOk.Location = New-Object System.Drawing.Point(($selectEditionForm.Width - 104), ($selectEditionForm.Height - 70))
$buttonOk.Size = New-Object System.Drawing.Size(75, 23)
$buttonOk.Text = '�������'
$selectEditionForm.Controls.Add($buttonOk)

$buttonOk.Add_Click({
    ([ref]$selectedResult).Value = $editionList[$editionListBox.SelectedIndex]
    $selectEditionForm.DialogResult = 'OK'
    $selectEditionForm.Close()
})
 
$local:buttonCancel = New-Object System.Windows.Forms.Button
$buttonCancel.Location = New-Object System.Drawing.Point(($selectEditionForm.Width - 199), ($selectEditionForm.Height - 70))
$buttonCancel.Size = New-Object System.Drawing.Size(75, 23)
$buttonCancel.Text = "������"
$selectEditionForm.Controls.Add($buttonCancel)

$buttonCancel.Add_Click({
    ([ref]$selectedResult).Value = $null
    $selectEditionForm.Close()
})

$local:selectedResult = $editionList[0]
$editionListBox.SelectedIndex = 0
$descriptionField.Text = $selectedResult.ImageDescription

$selectEditionForm.TopMost = $true
#$formResult = $selectEditionForm.ShowDialog()
$local:formResult = $selectEditionForm.ShowDialog((New-Object System.Windows.Forms.Form -Property @{TopMost = $true}))

if (-not ($formResult -eq 'OK')) {
    throw [System.EntryPointNotFoundException] '�� ������� �������� ��'
}

return $selectedResult

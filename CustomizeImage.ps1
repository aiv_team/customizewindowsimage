﻿param (
    [switch] $SkipMountISO = $false
)

################################################################################
# Переменные
#
# Папка для копирования файлов образа
$isoCopyFolder = Join-Path $PSScriptRoot 'isoCopy\'
# Папка для подключения образа
$offlineFolder = Join-Path $PSScriptRoot 'offline\'
# Папка со скриптами
$scriptsFolder = Join-Path $PSScriptRoot 'scripts'


###############################################################################
# Проверка прав
if (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) {
    Write-Warning "Недостаточно прав для выполнения этого скрипта. Откройте консоль PowerShell с правами администратора и запустите скрипт еще раз"
    Break
}


# Подключение системных UX/UI ресурсов
Add-Type -AssemblyName System.Windows.Forms


################################################################################
# Размонтирование образа ОС
function dismountImage {
    Param (
        [Parameter(Position=0)]
        $path = $null,
        [Parameter(Position=1)]
        $saveParam = 'Discard'
    )

    if ([string]::IsNullOrWhiteSpace($path)) {
        throw [System.IO.FileNotFoundException] 'Не указан путь к смонтированному образа ОС'
    }

    Write-Host 'Размонтируем образ ОС ' -NoNewline
    Write-Host $offlineFolder -ForegroundColor Cyan -NoNewline
    Write-Host '...'
    if ($saveParam -like 'Commit') {
        Dismount-WindowsImage -Path $offlineFolder -Save
    } else {
        Dismount-WindowsImage -Path $offlineFolder -Discard
    }
    Remove-Item -Path $offlineFolder -Recurse -Force
}


################################################################################
# Обработка образа ОС
function processImage {
    param (
        $path = $null
    )


    if ([string]::IsNullOrWhiteSpace($path)) {
        throw [System.IO.FileNotFoundException] 'Не указан путь к смонтированному образа ОС'
    }

    $private:selectionResult = $null

    ############################################################################
    # Обработка возможностей Windows
    $selectionResult = & "$scriptsFolder\Select-WindowsCapability.ps1" -path $offlineFolder
    if ($selectionResult -ne $null) {
		# todo разобраться со скачиванием доп.возможностей из интернета
        #forEach ($private:processedCapability in $selectionResult.CapabilitiesForInstall) {
        #    Write-Host 'Устанавливается ' -NoNewline
        #    Write-Host $processedCapability.DisplayName -ForegroundColor Cyan
        #    Add-WindowsCapability -Name $processedCapability.Name -Path $path
        #}
        forEach ($private:processedCapability in $selectionResult.CapabilitiesForRemove) {
            Write-Host 'Удаляется ' -NoNewline
            Write-Host $processedCapability.DisplayName -ForegroundColor Cyan
            Remove-WindowsCapability -Name $processedCapability.Name -Path $path
        }
    }

    ############################################################################
    # Обработка пакетов Windows
    $selectionResult = & "$scriptsFolder\Select-WindowsPackage.ps1" -path $offlineFolder
    if ($selectionResult -ne $null) {
		# todo разобраться со скачиванием пакетов из интернета
        #forEach ($private:processedPackage in $selectionResult.PackagesForInstall) {
        #    Write-Host 'Устанавливается ' -NoNewline
        #    Write-Host $processedPackage.PackageName -ForegroundColor Cyan
        #    Add-WindowsCapability -PackageName $processedPackage.PackageName -Path $path
        #}
        forEach ($private:processedPackage in $selectionResult.PackagesForRemove) {
            Write-Host 'Удаляется ' -NoNewline
            Write-Host $processedPackage.DisplayName -ForegroundColor Cyan
            Remove-WindowsPackage -PackageName $processedPackage.PackageName -Path $path
        }
    }

    ############################################################################
    # Обработка пакетов приложений
    $selectionResult = & "$scriptsFolder\Select-ProvisionedAppxPackages.ps1" -path $offlineFolder
    if ($selectionResult -ne $null) {
        forEach ($private:processedPackage in $selectionResult.PackagesForRemove) {
            Write-Host 'Удаляется ' -NoNewline
            Write-Host $processedPackage.DisplayName -ForegroundColor Cyan
            Remove-AppxProvisionedPackage -PackageName $processedPackage.PackageName -Path $path
        }
    }

    ############################################################################
    # Уменьшение объема образа
    Write-Host 'Уменьшаем объема образа...'
    Dism.exe /Cleanup-Image /Image:$offlineFolder /AnalyzeComponentStore
    Dism.exe /Cleanup-Image /Image:$offlineFolder /StartComponentCleanup /ResetBase
    Dism.exe /Cleanup-Image /Image:$offlineFolder /AnalyzeComponentStore
}


################################################################################
# Выбор образа ОС
try {
    if ($SkipMountISO) {
        $imageFileFullName = & "$scriptsFolder\Select-WimImageFile.ps1" -workFolder $PSScriptRoot -ErrorAction Stop
    } else {
        $imageFileFullName = & "$scriptsFolder\ExtractFiles-FromISO.ps1" -workFolder $PSScriptRoot -ErrorAction Stop
    }
} catch {
    Write-Error $_.Exception
    Break
}

if (Get-ItemPropertyValue -Path $imageFileFullName -Name IsReadOnly) {
    Set-ItemProperty -Path $imageFileFullName -Name IsReadOnly -Value $false
}
Write-Host 'Файл образа ' -NoNewline
Write-Host $imageFileFullName -ForegroundColor Cyan


################################################################################
# Выбор редакции ОС
try {
    $selectedEdition = & "$scriptsFolder\Select-WindowsEdition.ps1" -imageFileFullName $imageFileFullName
} catch {
    Write-Error $_.Exception
    Break
}


################################################################################
# Монтирование образа ОС
Write-Host 'Монтируем образ ОС ' -NoNewline
Write-Host $selectedEdition.ImageName -ForegroundColor Cyan -NoNewline
Write-Host '...'
try {
    if(Test-Path -Path $offlineFolder) {
        Remove-Item -Path $offlineFolder -Recurse -Force
    }
    $preventOutput = New-Item -Path $offlineFolder -ItemType Directory

    $offlineImage = Mount-WindowsImage -ImagePath $imageFileFullName -Index ($selectedEdition.ImageIndex) -Path $offlineFolder
} catch {
    Write-Error $_.Exception 
    Write-Host ('Попробуйте очистить информацию о повреждённых образах "Dism /Cleanup-Wim"', [System.Environment]::NewLine)
    Break
}


################################################################################
# Обработка образа ОС
try {
    processImage -path $offlineFolder
    dismountImage -path $offlineFolder -saveParam 'Commit'
} catch {
    Write-Error $_.Exception
    dismountImage -path $offlineFolder
    Break
}
